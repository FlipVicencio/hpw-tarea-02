function ejercicio10(arreglo){
    var objeto={};
    var a=arreglo.sort(ascendente);
    var x=0;
    objeto.mayor=a[a.length-1];
    objeto.menor=a[0];
    for(var i=0; i<a.length;i++){
        x+=a[i];
    }
    objeto.promedio=x/a.length;
    return objeto;
}
function ascendente(a,b){
    if(a<b) return -1;
    if(a>b) return 1;
    if(a==b) return 0;
}