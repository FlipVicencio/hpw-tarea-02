function ejercicio07(dia){
    var objeto={};
    var dia_anterior, dia_siquiente;
    var dias=["Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado"];
    for(var i=0;i<dias.length;i++){
        if(dia==dias[i]){
            objeto.dia_actual=dias[i];
            objeto.dia_anterior=dias[i-1];
            objeto.dia_siquiente=dias[i+1];
        }
    }
    return objeto;
}